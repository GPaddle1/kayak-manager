# Modèle logique

## People 

- User(<ins>idUser</ins>, firstName, lastname, birthDate, profilPictureLink, mailAddress, hashedPass, #idCountry)
- Athlete(<ins>idAthlete</ins>, firstName, lastname, birthDate, profilPictureLink, gender, #idCountry)

## Races

- Championship(<ins>idChampionship</ins>, championshipName, championshipDateBeginning, championshipDateEnd, #idChampionshipLevel, #idCountry)
- ChampionshipLevel(<ins>idChampionshipLevel</ins>, championshipLevelValue, championshipLevelDescription)
- Boat(<ins>idBoat</ins>, boatName, boatType)
- Race(<ins>idRace</ins>, raceName, raceDate, raceDistance, idChampionship)
- Result(<ins>idResult</ins>, hValue, mValue, sValue, #idChampionship, #idRace, #idBoat)

## Link

- Team(<ins>idTeam</ins>, teamName, #idUser)

## General data

- Country(<ins>idCountry</ins>, countryName, flagImg, flagEmoji, flagTxt)
- RacesMetadata(<ins>idRacesMetadata</ins>, racesMetadataValue, racesMetadataDescription, #idRace, #idMetadataType)
- ChampionshipsMetadata(<ins>idChampionshipsMetadata</ins>, championshipsMetadataValue, championshipsMetadataDescription, #idChampionship, #idMetadataType)
- AthletesMetadata(<ins>idAthletesMetadata</ins>, athletesMetadataValue, athletesMetadataDescription, #idAthlete, #idMetadataType)
- MetadataType(<ins>idMetadataType</ins>, metadataTypeDescription, regexVerification)

## * ... * Association

- RaceComposition(<ins>#idBoat, #idRace</ins>)
- BoatComposition(<ins>#idAthlete, #idBoat</ins>)
- TeamAthletes(<ins>#idAthlete, #idTeam</ins>)
- AthletesResults(<ins>#idAthlete, #idResult</ins>)
- BoatResults(<ins>#idAthlete, #idResult</ins>)
- RaceResults(<ins>#idRace, #idResult</ins>)