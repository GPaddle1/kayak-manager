# KAYAK MANAGER

[[_TOC_]]

## 🎴 CONTEXTE
Dans l’idée de la plateforme Tour de France Fantasy ou autres jeux de stratégie managériale, l’idée serait de mettre en place une plateforme pour ajouter de l’interactivité aux compétitions. Ainsi avant une compétition, le public constitue une équipe, selon certaines contraintes (Crédit attribué aux sportifs selon les résultats, catégories, …).

Durant la compétition, le public pourra consulter les résultats de ses sportifs, et verra ses points évoluer tout du long de l’évènement.

A la fin d’un évènement, un classement décriera les retours de chaque joueur afin de proposer un classement final et récompenser les plus fins visionnaires.

Les jeux d’argents pouvant conduire à de nombreuses dérives, ce jeu, où aucune valeur marchande ne sera échangé, sera une bonne manière d’intéresser et de gamifier le monde assez fermé et stricte du sport amateur.

## 🎯 OBJECTIF ET PUBLIC VISE

<details>
<summary>Détails</summary>
Afin de rendre les temps morts en compétition plus festif, et créer un engouement nouveau dans ces temps de compétition, « Kayak manager » veut devenir un lien entre le jeu et le sérieux des compétitions.

Tout le monde peut se lancer dans le jeu. Le novice pourra tenter des combinaisons aléatoires tandis que l’expert analysera les données passées insérées dans la base de « Kayak manager »

Bien souvent le compétiteur n’a à cœur que ses propres résultats, cependant, il est important de se considérer comme une communauté, et de ce fait, l’intéressement aux autres catégories (Pour engranger des points notamment) est primordial.

La gamification du sujet permettra de donner de la visibilité à la discipline et pourquoi pas, convertir des publics joueurs à la discipline.

Le concept est éprouvé notamment avec le Tour de France qui a déjà regroupé plus de 160.000 joueurs en 2020.

Le kayak de course en ligne et de marathon sont des disciplines qui pourraient facilement se voir assignées des retours identiques au vélo de par la ressemblance (classement numérique entre de nombreux participants, assignation facile de points en fonction d’un classement, …)
</details>


## 📖 TERMINOLOGIE
<details>
<summary>Détails</summary>

- Kayak (K) : Sport nautique dans lequel le sportif est assis et utilise une pagaie double 
-	Canoë (C) : Sport nautique dans lequel le sportif est à genoux et utilise une pagaie simple
----------
-	Points : Valeur arbitraire donnée pour récompenser les résultats d’un sportif
-	Crédits : valeur fictive attribuée à un sportif selon ses résultats passés, cette valeur permettra de donner une limite aux équipes (X Crédits max à dépenser pour constituer son équipe)
----------
-	WCH : World Championship = Championnats du monde 
-	WCP : World Cup = Coupe du monde 
-	OG : Olympic Games = Jeux olympiques
----------
-	Course en ligne : discipline du Canoë/Kayak consistant à effectuer le plus vite possible une des 3 distances (200m/500m/1000m) en canoë ou en kayak dans une des embarcations dédiées (monoplace : K1/C1 ; biplace : K2/C2 ; quatre places : K4/C4)
-	Marathon : Course longue en Kayak ou en Canoë, monoplace ou biplace (25 à 30 km) entrecoupée de phases de course à pied avec son bateau afin de se ravitailler 
</details>


## 🔨 SCENARIOS

<details>
<summary>Détails</summary>

### RECUPERATION DES SPORTIFS
- L’utilisateur a besoin de connaitre l’ensemble des sportifs participant à une compétition, avec l’identifiant de la compétition, on pourra retrouver l’intégralité des sportifs présents.
### LISTE DES COMPETITIONS
- Un utilisateur peut vouloir la liste des compétitions disponibles dans le système.
### LISTE DES COMPETITIONS A VENIR
- Un tri peut être effectué sur la liste des compétitions précédemment obtenu afin de ne trouver que les compétitions à venir pour y constituer une équipe.
### LISTE DES RESULTATS
- Les résultats sont accessibles afin de pouvoir faire une correspondance sportif/nombre de points obtenus
### OBTENTION DES SCORES
- Une fois les résultats finaux édités, chaque place se voit dotée d’un nombre de points relatif à la place obtenu
### CLASSEMENT DES EQUIPES
- Une équipe, en fonction de sa composition, se verra dotée d’un certain nombre de points. Le cumul permet un classement final des équipes en fonction des points cumulées par chacune d’entre elles.

---------

### RECUPERATION DES RESULTATS D’UN SPORTIF
- L’application, disposant d’une base de données intéressante peut servir de source pour connaitre les résultats des différentes courses par athlète. De ce fait il est facile de retracer le palmarès d’un athlète à l’aide de ces données structurées et exploitables.
### RECUPERATION DES RESULTATS D’UNE NATION
- De la même manière que la récupération des résultats individuels, le groupement des résultats par nation est possible afin de proposer un récapitulatif de ce qui a été effectué durant une compétition.
### EDITION DE PALMARES SUR UNE SAISON
- Il est possible de combiner les filtres précédents afin d’obtenir un retour sur la saison sportive au complet.
### EDITION DE « RECORDS »
- Une fois les données de l’année intégrées dans le système il est possible de récupérer les meilleurs chronos par distance, par catégorie (K/C) (H/F).
### TABLEAU DES MEDAILLES
- La connaissance des résultats permet d’éditer un tableau des médailles (Par compétition, par catégorie, …).
### ATHLETES LES PLUS TITRES
- Il est possible d’obtenir le/les athlètes les plus titrés (par évènement, par saison, …).
### ATHLETES LES PLUS REGULIERS
- Il est possible de juger la régularité des sportifs à la connaissance de leurs résultats.

---------

### RECUPERATION DES CLASSEMENTS DES JOUEURS
- Une fois une compétition passée il est possible d’établir un classement des joueurs avec des équipes complètes.
### RECUPERATION DU CLASSEMENT D’UN JOUEUR
- A la fin d’une compétition, le joueur peut connaitre son score.
### AFFICHAGE DU DETAIL D’UNE EQUIPE
- Après la parution des résultats, le joueur peut découvrir le détail de son équipe (Résultat + nombres de points de chaque sportifs).
### CREATION D’UNE EQUIPE 
- Un joueur peut insérer une équipe de 6 sportifs en base afin de constituer son équipe.
### DECOUVERTE D’UN SPORTIF
- La base de données permet l’accès à un sportif de manière aléatoire afin de découvrir des profils parfois inconnus.
### MODIFICATION DES DONNEES D’UN JOUEUR
- Un joueur peut librement accéder aux données de son compte, ainsi il est capable de modifier ses données en connaissant son ID et un mot de passe 
</details>

## 🚛 ESTIMATION DE LA TAILLE DES DONNEES

<details>
<summary>Détails</summary>
La base de données devrait comprendre environ :
-	400 athlètes 
-	100 pays
-	2700 résultats
-	2700 bateaux
-	900 courses
-	5 championnats

Cela représente donc :

|  				|Taille unitaire| |Quantité estimée	| 		|Total		|	|	|
|-				|-				|-|-				|-		|-|-			|-	|
|Athlètes		|406 			|o|400				|162400	|o|	0.1624		|Mo	|
|Pays			|310 			|o|100				|31000	|o|	0.031000	|Mo	|
|Résultats		|4 				|o|2700				|10800	|o|	0.010800	|Mo	|
|Bateaux		|21 			|o|2700				|56700	|o|	0.056700	|Mo	|
|Courses		|104 			|o|900				|93600	|o|	0.093600	|Mo	|
|Championnats	|104 			|o|5				|520	|o|	0.000520	|Mo	|
|Total			| 				| | 				|355020	|o|	0.355020	|Mo	|

A cela s’ajoutera les enregistrements des utilisateurs que nous pouvons estimer à 406 000 o (0.406 Mo) pour 1000 enregistrements (406 o par enregistrement)

</details>

### Schéma logique

Le schéma logique de la base de données est disponible [à cette addresse](./docs/ModeleLogique.md)

--------

## Schéma de la base 


[![Modèle](./img/KayakManager.svg)](./img/KayakManager.svg)

## 💻 Utilisation des scripts

Initialement, le travail a été effectué sur une machine Docker.
Pour exécuter les insertions il est important de conserver la structure interne du répertoire : 

```
- KayakManager/
	- data/
	- scripts/
```

Au moment de la connection à la base de données il faut ajouter `--local-infile=1` (Comme présenté ci-dessous) ceci permettra d'utiliser les commandes d'insertion de données.

`mysql -u<username> -p<password> <database> --local-infile=1`

Par la suite les scripts pourront être lancés à l'aide de la commande `source <NomDuScript.sql>`

### Création des tables

La création des tables s'effectue à l'aide du [script CreateTables.sql](./scripts/CreateTables.sql)

`source CreateTables.sql`

--------

### Insertion des données

L'insertion des données s'effectue à l'aide du [script InsertData.sql](./scripts/InsertData.sql)

`source InsertData.sql`

--------
### Exécution des requêtes

L'exécution des requêtes s'effectue à l'aide du [script Requetes.sql](./scripts/Requetes.sql)

`source Requetes.sql`

--------
### Exécution des procédures

L'exécution des procédures s'effectue à l'aide du [script Procedures.sql](./scripts/Procedures.sql)

`source Procedures.sql`

## Résumé 

Dans l'ordre les commandes sont les suivantes : 

```bash
	git clone https://gitlab.com/GPaddle1/kayak-manager.git
	cd scripts
	mysql -u<username> -p<password> <database> --local-infile=1
	<source DropTables.sql>
	source CreateTables.sql
	source InsertData.sql
	source Requetes.sql
	source Procedures.sql
```

## Crédits 🖋

Keller Guillaume @EI-CNAM FIP1A 2020-2021
