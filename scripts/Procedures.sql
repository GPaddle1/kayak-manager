select
	'-- RECUPERATION DU CLASSEMENT D’UN JOUEUR';

-- RECUPERATION DU CLASSEMENT D’UN JOUEUR ✅
-- Procedure

DROP PROCEDURE IF Exists getPlayerRank;

DELIMITER | 
CREATE PROCEDURE getPlayerRank (IN idPlayer INT) 
	BEGIN

		create table `TMP_RankingTable`(
			idUser INT Primary key,
			userName Varchar(200),
			points Decimal(6, 2),
			ranking INT
		);

		set
			@row_num = 0;

		insert into
			`TMP_RankingTable`
		select
			T1.*,
			@row_num := @row_num + 1 as row_index
		from
			(
				select
					User.idUser,
					CONCAT(User.firstName, " ", User.lastName) as UserName,
					100 / AVG(Result.raceResult) as Points
				from
					User
					inner join Team on Team.idUser = User.idUser
					inner join TeamAthletes on TeamAthletes.idTeam = Team.idTeam
					inner join Athlete on Athlete.idAthlete = TeamAthletes.idAthlete
					inner join AthletesResults on AthletesResults.idAthlete = Athlete.idAthlete
					inner join Result on Result.idResult = AthletesResults.idResult
				group by
					User.idUser
				order by
					Points DESC
			) as T1;

		select
			*
		from
			`TMP_RankingTable`
		where
			TMP_RankingTable.idUser = idPlayer;

		drop table
			`TMP_RankingTable`;

	END 
	| 
delimiter ;

call getPlayerRank(28);

select
	'-- RECUPERATION D UN JOUEUR SELON SON CLASSEMENT';

-- RECUPERATION D UN JOUEUR SELON SON CLASSEMENT ✅
-- Procedure

DROP PROCEDURE IF Exists getPlayerByRank;

DELIMITER | 
CREATE PROCEDURE getPlayerByRank (IN playerRanking INT) 
	BEGIN

		create table `TMP_RankingTable`(
			idUser INT Primary key,
			userName Varchar(200),
			points Decimal(6, 2),
			ranking INT
		);

		set
			@row_num = 0;

		insert into
			`TMP_RankingTable`
		select
			T1.*,
			@row_num := @row_num + 1 as row_index
		from
			(
				select
					User.idUser,
					CONCAT(User.firstName, " ", User.lastName) as UserName,
					100 / AVG(Result.raceResult) as Points
				from
					User
					inner join Team on Team.idUser = User.idUser
					inner join TeamAthletes on TeamAthletes.idTeam = Team.idTeam
					inner join Athlete on Athlete.idAthlete = TeamAthletes.idAthlete
					inner join AthletesResults on AthletesResults.idAthlete = Athlete.idAthlete
					inner join Result on Result.idResult = AthletesResults.idResult
				group by
					User.idUser
				order by
					Points DESC
			) as T1;

		select
			*
		from
			`TMP_RankingTable`
		where
			TMP_RankingTable.ranking = playerRanking;

		drop table
			`TMP_RankingTable`;

	END 
	| 
delimiter ;

call getPlayerByRank(4);