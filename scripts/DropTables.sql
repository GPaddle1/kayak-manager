SELECT
	'Drop tables';

Drop table if exists `TeamAthletes`;

Drop table if exists `Team`;

Drop table if exists `User`;

ALTER TABLE
	`Race` DROP FOREIGN KEY Race_idChampionship_fk;

Drop table if exists `AthletesResults`;

Drop table if exists `BoatResults`;

Drop table if exists `RaceResults`;

Drop table if exists `RaceComposition`;

Drop table if exists `BoatComposition`;

Drop table if exists `RacesMetadata`;

Drop table if exists `ChampionshipsMetadata`;

Drop table if exists `AthletesMetadata`;

Drop table if exists `MetadataType`;

Drop table if exists `Result`;

Drop table if exists `Boat`;

Drop table if exists `Championship`;

Drop table if exists `ChampionshipLevel`;

Drop table if exists `Athlete`;

Drop table if exists `Race`;

Drop table if exists `Country`;