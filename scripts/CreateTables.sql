SELECT
	'Create tables';

-- ✅
CREATE TABLE `Country`(
	`idCountry` INT Auto_Increment Not null Primary key,
	`countryName` Varchar(100) Not null,
	`flagTxt` Varchar(100) Not null,
	`flagEmoji` Varchar(100),
	`flagImg` Varchar(100)
) CHARACTER SET latin1 COLLATE latin1_bin;

-- ✅
CREATE TABLE `User`(
	`idUser` INT Auto_Increment Not null Primary key,
	`firstName` Varchar(100) Not null,
	`lastName` Varchar(100) Not null,
	`birthDate` Datetime Not null,
	`mailAddress` Varchar(200) Not null,
	`idCountry` INT Not null,
	`hashedPass` Varchar(200),
	`profilPictureLink` Varchar(200),
	CHECK (mailAddress LIKE '%@%.%')
) CHARACTER SET latin1 COLLATE latin1_bin;

ALTER TABLE
	`User`
ADD
	CONSTRAINT User_idCountry_fk FOREIGN KEY (idCountry) REFERENCES `Country`(idCountry);

-- ✅
CREATE TABLE `Athlete`(
	`idAthlete` INT Auto_Increment Not null Primary key,
	`firstName` Varchar(100) Not null,
	`lastname` Varchar(100) Not null,
	`idCountry` INT Not null,
	`birthDate` Datetime,
	`profilPictureLink` Varchar(200),
	`gender` TINYINT,
	UNIQUE(firstName, lastname, idCountry)
) CHARACTER SET latin1 COLLATE latin1_bin;

ALTER TABLE
	`Athlete`
ADD
	CONSTRAINT Athlete_idCountry_fk FOREIGN KEY (idCountry) REFERENCES `Country`(idCountry);

-- ✅
CREATE TABLE `ChampionshipLevel`(
	`idChampionshipLevel` INT Auto_Increment Not null Primary key,
	`championshipLevelValue` INT Not null,
	`championshipLevelDescription` Varchar(200)
) CHARACTER SET latin1 COLLATE latin1_bin;

-- ✅
CREATE TABLE `Championship`(
	`idChampionship` INT Auto_Increment Not null Primary key,
	`championshipName` Varchar(100) Not null,
	`championshipDateBeginning` Datetime Not null,
	`championshipDateEnd` Datetime Not null,
	`idChampionshipLevel` INT Not null,
	`idCountry` INT Not null
) CHARACTER SET latin1 COLLATE latin1_bin;

ALTER TABLE
	`Championship`
ADD
	CONSTRAINT Championship_idChampionshipLevel_fk FOREIGN KEY (idChampionshipLevel) REFERENCES `ChampionshipLevel`(idChampionshipLevel);

ALTER TABLE
	`Championship`
ADD
	CONSTRAINT Championship_idCountry_fk FOREIGN KEY (idCountry) REFERENCES `Country`(idCountry);

-- ✅
CREATE TABLE `Race`(
	`idRace` INT Auto_Increment Not null Primary key,
	`raceName` Varchar(100),
	`raceDate` Datetime Not null,
	`raceDistance` Int Not null,
	`idChampionship` INT Not null,
	UNIQUE(raceName, raceDate)
) CHARACTER SET latin1 COLLATE latin1_bin;

ALTER TABLE
	`Race`
ADD
	CONSTRAINT Race_idChampionship_fk FOREIGN KEY (idChampionship) REFERENCES `Championship`(idChampionship);

-- ✅
CREATE TABLE `Boat`(
	`idBoat` INT Auto_Increment Not null Primary key,
	`boatName` Varchar(100),
	`boatType` Enum("Kayak", "Canoe") Not null
) CHARACTER SET latin1 COLLATE latin1_bin;

-- ✅
CREATE TABLE `Result`(
	`idResult` INT Auto_Increment Not null Primary key,
	`raceResult` Int,
	`hValue` Int,
	`mValue` Int,
	`sValue` Decimal(5, 3),
	`idChampionship` INT,
	`idRace` INT,
	`idBoat` INT,
	Check(
		sValue < 60
		AND sValue >= 0
	),
	Check(
		mValue < 60
		AND mValue >= 0
	)
) CHARACTER SET latin1 COLLATE latin1_bin;

ALTER TABLE
	`Result`
ADD
	CONSTRAINT Result_idRace_fk FOREIGN KEY (idRace) REFERENCES `Race`(idRace);

ALTER TABLE
	`Result`
ADD
	CONSTRAINT Result_idChampionship_fk FOREIGN KEY (idChampionship) REFERENCES `Championship`(idChampionship);

ALTER TABLE
	`Result`
ADD
	CONSTRAINT Result_idBoat_fk FOREIGN KEY (idBoat) REFERENCES `Boat`(idBoat) ON DELETE CASCADE;

/*
 ALTER TABLE
 `Boat`
 ADD
 CONSTRAINT Boat_idResult_fk FOREIGN KEY (idResult) REFERENCES `Result`(idResult);
 */
-- ✅
CREATE TABLE `Team`(
	`idTeam` INT Auto_Increment Not null Primary key,
	`teamName` Varchar(100),
	`idUser` INT Not null
) CHARACTER SET latin1 COLLATE latin1_bin;

ALTER TABLE
	`Team`
ADD
	CONSTRAINT Team_idUser_fk FOREIGN KEY (idUser) REFERENCES `User`(idUser);

-- ✅
CREATE TABLE `MetadataType`(
	`idMetadataType` INT Auto_Increment Not null Primary key,
	`MetadataTypeDescription` Varchar(300),
	`regexVerification` Varchar(200)
) CHARACTER SET latin1 COLLATE latin1_bin;

-- ✅
CREATE TABLE `RacesMetadata`(
	`idRacesMetadata` INT Auto_Increment Not null Primary key,
	`racesMetadataValue` Varchar(200) Not null,
	`racesMetadataDescription` Varchar(100) Not null,
	`idRace` INT Not null,
	`idMetadataType` INT Not null
) CHARACTER SET latin1 COLLATE latin1_bin;

ALTER TABLE
	`RacesMetadata`
ADD
	CONSTRAINT RacesMetadata_idMetadataType_fk FOREIGN KEY (idMetadataType) REFERENCES `MetadataType`(idMetadataType);

ALTER TABLE
	`RacesMetadata`
ADD
	CONSTRAINT RacesMetadata_idRace_fk FOREIGN KEY (idRace) REFERENCES `Race`(idRace);

-- ✅
CREATE TABLE `ChampionshipsMetadata`(
	`idChampionshipsMetadata` INT Auto_Increment Not null Primary key,
	`championshipsMetadataValue` Varchar(200) not null,
	`championshipsMetadataDescription` Varchar(100) not null,
	`idChampionship` INT not null,
	`idMetadataType` INT not null
) CHARACTER SET latin1 COLLATE latin1_bin;

ALTER TABLE
	`ChampionshipsMetadata`
ADD
	CONSTRAINT ChampionshipsMetadata_idMetadataType_fk FOREIGN KEY (idMetadataType) REFERENCES `MetadataType`(idMetadataType);

ALTER TABLE
	`ChampionshipsMetadata`
ADD
	CONSTRAINT ChampionshipsMetadata_idChampionship_fk FOREIGN KEY (idChampionship) REFERENCES `Championship`(idChampionship);

-- ✅
CREATE TABLE `AthletesMetadata`(
	`idAthletesMetadata` INT Auto_Increment Not null Primary key,
	`athletesMetadataValue` Varchar(200) not null,
	`athletesMetadataDescription` Varchar(100) not null,
	`idAthlete` INT not null,
	`idMetadataType` INT not null
) CHARACTER SET latin1 COLLATE latin1_bin;

ALTER TABLE
	`AthletesMetadata`
ADD
	CONSTRAINT AthletesMetadata_idMetadataType_fk FOREIGN KEY (idMetadataType) REFERENCES `MetadataType`(idMetadataType);

ALTER TABLE
	`AthletesMetadata`
ADD
	CONSTRAINT AthletesMetadata_idAthlete_fk FOREIGN KEY (idAthlete) REFERENCES `Athlete`(idAthlete);

-- ✅
CREATE TABLE `RaceComposition`(
	`idBoat` INT,
	`idRace` INT,
	Primary key (idBoat, idRace)
) CHARACTER SET latin1 COLLATE latin1_bin;

ALTER TABLE
	`RaceComposition`
ADD
	CONSTRAINT RaceComposition_idRace_fk FOREIGN KEY (idRace) REFERENCES `Race`(idRace);

ALTER TABLE
	`RaceComposition`
ADD
	CONSTRAINT RaceComposition_idBoat_fk FOREIGN KEY (idBoat) REFERENCES `Boat`(idBoat);

-- ✅
CREATE TABLE `BoatComposition`(
	`idAthlete` INT,
	`idBoat` INT,
	Primary key (idAthlete, idBoat)
) CHARACTER SET latin1 COLLATE latin1_bin;

ALTER TABLE
	`BoatComposition`
ADD
	CONSTRAINT BoatComposition_idBoat_fk FOREIGN KEY (idBoat) REFERENCES `Boat`(idBoat);

ALTER TABLE
	`BoatComposition`
ADD
	CONSTRAINT BoatComposition_idAthlete_fk FOREIGN KEY (idAthlete) REFERENCES `Athlete`(idAthlete);

-- ✅
CREATE TABLE `TeamAthletes`(
	`idAthlete` INT,
	`idTeam` INT,
	Primary key (idAthlete, idTeam)
) CHARACTER SET latin1 COLLATE latin1_bin;

ALTER TABLE
	`TeamAthletes`
ADD
	CONSTRAINT TeamAthletes_idTeam_fk FOREIGN KEY (idTeam) REFERENCES `Team`(idTeam);

ALTER TABLE
	`TeamAthletes`
ADD
	CONSTRAINT TeamAthletes_idAthlete_fk FOREIGN KEY (idAthlete) REFERENCES `Athlete`(idAthlete);

-- ✅
CREATE TABLE `BoatResults`(
	`idBoat` INT,
	`idResult` INT,
	Primary key (idBoat, idResult)
) CHARACTER SET latin1 COLLATE latin1_bin;

ALTER TABLE
	`BoatResults`
ADD
	CONSTRAINT BoatResults_idResult_fk FOREIGN KEY (idResult) REFERENCES `Result`(idResult);

ALTER TABLE
	`BoatResults`
ADD
	CONSTRAINT BoatResults_idBoat_fk FOREIGN KEY (idBoat) REFERENCES `Boat`(idBoat);

-- ✅
CREATE TABLE `AthletesResults`(
	`idAthlete` INT,
	`idResult` INT,
	Primary key (idAthlete, idResult)
) CHARACTER SET latin1 COLLATE latin1_bin;

ALTER TABLE
	`AthletesResults`
ADD
	CONSTRAINT AthletesResults_idAthlete_fk FOREIGN KEY (idAthlete) REFERENCES `Athlete`(idAthlete);

ALTER TABLE
	`AthletesResults`
ADD
	CONSTRAINT AthletesResults_idResult_fk FOREIGN KEY (idResult) REFERENCES `Result`(idResult);

-- ✅
CREATE TABLE `RaceResults`(
	`idRace` INT,
	`idResult` INT,
	Primary key (idRace, idResult)
) CHARACTER SET latin1 COLLATE latin1_bin;

ALTER TABLE
	`RaceResults`
ADD
	CONSTRAINT RaceResults_idRace_fk FOREIGN KEY (idRace) REFERENCES `Race`(idRace);

ALTER TABLE
	`RaceResults`
ADD
	CONSTRAINT RaceResults_idResult_fk FOREIGN KEY (idResult) REFERENCES `Result`(idResult)