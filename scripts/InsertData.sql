SELECT
	'Insert data';

SET GLOBAL local_infile=1;

delete from `Country`;
select "INTO TABLE `Country`";
LOAD DATA LOCAL INFILE '../data/Country.csv' 
INTO TABLE `Country`
FIELDS TERMINATED BY ";"
LINES TERMINATED BY "\r\n"
IGNORE 1 LINES
(countryName, flagTxt, idCountry);

-- Show warnings;
-- Select * from `Country`;

delete from `User`;
select "INTO TABLE `User`";
LOAD DATA LOCAL INFILE '../data/User.csv' 
INTO TABLE `User`
FIELDS TERMINATED BY ";"
LINES TERMINATED BY "\r\n"
IGNORE 1 LINES
(firstName, lastName, birthDate, mailAddress, idCountry, idUser);
-- Show warnings;
-- Select * from `User`;

delete from `Athlete`;
select "INTO TABLE `Athlete`";
LOAD DATA LOCAL INFILE '../data/Athlete.csv' 
INTO TABLE `Athlete`
FIELDS TERMINATED BY ";"
LINES TERMINATED BY "\r\n"
IGNORE 1 LINES
(firstName, lastname, idCountry, @tmp1, @tmp2,idAthlete);
Show warnings;
-- Select * from `Athlete`;

delete from `ChampionshipLevel`;
select "INTO TABLE `ChampionshipLevel`";
LOAD DATA LOCAL INFILE '../data/ChampionshipLevel.csv' 
INTO TABLE `ChampionshipLevel`
FIELDS TERMINATED BY ";"
LINES TERMINATED BY "\r\n"
IGNORE 1 LINES
(championshipLevelValue, championshipLevelDescription);
-- Show warnings;
-- Select * from `ChampionshipLevel`;

delete from `Championship`;
select "INTO TABLE `Championship`";
LOAD DATA LOCAL INFILE '../data/Championship.csv' 
INTO TABLE `Championship`
FIELDS TERMINATED BY ";"
LINES TERMINATED BY "\r\n"
IGNORE 1 LINES
(idChampionship, championshipName, championshipDateBeginning, championshipDateEnd, idChampionshipLevel, idCountry, @tmp);
-- Show warnings;
-- Select * from `Championship`;

delete from `Race`;
select "INTO TABLE `Race`";
LOAD DATA LOCAL INFILE '../data/Race.csv' 
INTO TABLE `Race`
FIELDS TERMINATED BY ";"
LINES TERMINATED BY "\r\n"
IGNORE 1 LINES
(idRace, raceName, raceDate, raceDistance, @tmp1, @tmp2, @tmp3, @tmp4, @tmp5, idChampionship);
-- Show warnings;
-- Select * from `Race`;

delete from `Boat`;
select "INTO TABLE `Boat`";
LOAD DATA LOCAL INFILE '../data/Boat.csv' 
INTO TABLE `Boat`
FIELDS TERMINATED BY ";"
LINES TERMINATED BY "\r\n"
IGNORE 1 LINES
(boatName, idBoat, boatType);
-- Show warnings;
-- Select * from `Boat`;

delete from `Result`;
select "INTO TABLE `Result`";
LOAD DATA LOCAL INFILE '../data/Result.csv' 
INTO TABLE `Result`
FIELDS TERMINATED BY ";"
LINES TERMINATED BY "\r\n"
IGNORE 1 LINES
(idResult, raceResult, hValue, mValue, sValue, idChampionship, idRace, idBoat, @tmp1, @tmp2);
-- Show warnings;
-- Select * from `Result`;

delete from `Team`;
select "INTO TABLE `Team`";
LOAD DATA LOCAL INFILE '../data/Team.csv' 
INTO TABLE `Team`
FIELDS TERMINATED BY ";"
LINES TERMINATED BY "\r\n"
IGNORE 1 LINES
(teamName, idUser, idTeam);
-- Show warnings;
-- Select * from `Team`;

delete from `MetadataType`;
select "INTO TABLE `MetadataType`";
LOAD DATA LOCAL INFILE '../data/MetadataType.csv' 
INTO TABLE `MetadataType`
FIELDS TERMINATED BY ";"
LINES TERMINATED BY "\r\n"
IGNORE 1 LINES
(idMetadataType, MetadataTypeDescription, regexVerification);
-- Show warnings;
-- Select * from `MetadataType`;

delete from `RacesMetadata`;
select "INTO TABLE `RacesMetadata`";
LOAD DATA LOCAL INFILE '../data/RacesMetadata.csv' 
INTO TABLE `RacesMetadata`
FIELDS TERMINATED BY ";"
LINES TERMINATED BY "\r\n"
IGNORE 1 LINES
(idRacesMetadata, racesMetadataValue, racesMetadataDescription, idRace, idMetadataType);
-- Show warnings;
-- Select * from `RacesMetadata`;

delete from `ChampionshipsMetadata`;
select "INTO TABLE `ChampionshipsMetadata`";
LOAD DATA LOCAL INFILE '../data/ChampionshipsMetadata.csv' 
INTO TABLE `ChampionshipsMetadata`
FIELDS TERMINATED BY ";"
LINES TERMINATED BY "\r\n"
IGNORE 1 LINES
(idChampionshipsMetadata, championshipsMetadataValue, championshipsMetadataDescription, idChampionship, idMetadataType);
-- Show warnings;
-- Select * from `ChampionshipsMetadata`;

delete from `AthletesMetadata`;
select "INTO TABLE `AthletesMetadata`";
LOAD DATA LOCAL INFILE '../data/AthletesMetadata.csv' 
INTO TABLE `AthletesMetadata`
FIELDS TERMINATED BY ";"
LINES TERMINATED BY "\r\n"
IGNORE 1 LINES
(idAthletesMetadata, athletesMetadataValue, athletesMetadataDescription, idAthlete, idMetadataType);
-- Show warnings;
-- Select * from `AthletesMetadata`;

delete from `RaceComposition`;
select "INTO TABLE `RaceComposition`";
LOAD DATA LOCAL INFILE '../data/RaceComposition.csv' 
INTO TABLE `RaceComposition`
FIELDS TERMINATED BY ";"
LINES TERMINATED BY "\r\n"
IGNORE 1 LINES
(idBoat, idRace);
-- Show warnings;
-- Select * from `RaceComposition`;

delete from `BoatComposition`;
select "INTO TABLE `BoatComposition`";
LOAD DATA LOCAL INFILE '../data/BoatComposition.csv' 
INTO TABLE `BoatComposition`
FIELDS TERMINATED BY ";"
LINES TERMINATED BY "\r\n"
IGNORE 1 LINES
(idBoat, idAthlete);
-- Show warnings;
-- Select * from `BoatComposition`;

delete from `TeamAthletes`;
select "INTO TABLE `TeamAthletes`";
LOAD DATA LOCAL INFILE '../data/TeamAthletes.csv' 
INTO TABLE `TeamAthletes`
FIELDS TERMINATED BY ";"
LINES TERMINATED BY "\r\n"
IGNORE 1 LINES
(idAthlete, idTeam);
-- Show warnings;
-- Select * from `TeamAthletes`;

delete from `BoatResults`;
select "INTO TABLE `BoatResults`";
LOAD DATA LOCAL INFILE '../data/BoatResults.csv' 
INTO TABLE `BoatResults`
FIELDS TERMINATED BY ";"
LINES TERMINATED BY "\r\n"
IGNORE 1 LINES
(idBoat, idResult);
-- Show warnings;
-- Select * from `BoatResults`;

delete from `AthletesResults`;
select "INTO TABLE `AthletesResults`";
LOAD DATA LOCAL INFILE '../data/AthletesResults.csv' 
INTO TABLE `AthletesResults`
FIELDS TERMINATED BY ";"
LINES TERMINATED BY "\r\n"
IGNORE 1 LINES
(idAthlete, idResult, @tmp1, @tmp2);
-- Show warnings;
-- Select * from `AthletesResults`;

delete from `RaceResults`;
select "INTO TABLE `RaceResults`";
LOAD DATA LOCAL INFILE '../data/RaceResults.csv' 
INTO TABLE `RaceResults`
FIELDS TERMINATED BY ";"
LINES TERMINATED BY "\r\n"
IGNORE 1 LINES
(idRace, idResult);
-- Show warnings;
-- Select * from `RaceResults`;