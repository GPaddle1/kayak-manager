select
	'-- RECUPERATION DES SPORTIFS';

-- RECUPERATION DES SPORTIFS ✅
Select
	idAthlete,
	firstName,
	lastName
from
	Athlete;

select
	'-- RECUPERATION DES KAYAKS';

-- RECUPERATION DES KAYAKS ✅
Select
	Boat.idBoat,
	Boat.boatName,
	Boat.boatType
from
	Boat
	inner join BoatComposition on Boat.idBoat = BoatComposition.idBoat
	inner join Athlete on Athlete.idAthlete = BoatComposition.idAthlete
where
	boatType = "Kayak";

select
	'-- RECUPERATION DES CANOES';

-- RECUPERATION DES CANOES ✅
Select
	Boat.idBoat,
	Boat.boatName,
	Boat.boatType
from
	Boat
	inner join BoatComposition on Boat.idBoat = BoatComposition.idBoat
	inner join Athlete on Athlete.idAthlete = BoatComposition.idAthlete
where
	boatType = "Canoe";

select
	'-- LISTE DES COMPETITIONS';

-- LISTE DES COMPETITIONS ✅
Select
	championshipName,
	championshipDateBeginning,
	championshipDateEnd
from
	Championship;

select
	'-- LISTE DES COMPETITIONS A VENIR';

-- LISTE DES COMPETITIONS A VENIR ✅
Select
	*
from
	Championship
where
	championshipDateBeginning > CURDATE();

select
	'-- LISTE DES RESULTATS';

-- LISTE DES RESULTATS ✅
Select
	CONCAT(hValue, ":", mValue, "'", sValue) as Duree,
	raceResult
from
	Result
order by
	raceResult;

select
	'-- RECUPERATION DES RESULTATS D’UN SPORTIF';

-- RECUPERATION DES RESULTATS D’UN SPORTIF ✅
select
	Concat(firstName, " ", lastName) as "Athlete name",
	Race.raceDistance,
	CONCAT(hValue, ":", mValue, "'", sValue) as Duree
from
	Athlete
	inner join AthletesResults on AthletesResults.idAthlete = Athlete.idAthlete
	inner join Result on Result.idResult = AthletesResults.idResult
	inner join RaceResults on RaceResults.idResult = Result.idResult
	inner join Race on Race.idRace = RaceResults.idRace
group by
	Athlete.idAthlete,
	raceDistance,
	firstName,
	lastName,
	Result.raceResult,
	hValue,
	mValue,
	sValue
order by
	Athlete.idAthlete,
	Race.raceDistance,
	sValue + 60 *(mValue + 60 * hValue);

select
	'-- RECUPERATION DES RESULTATS D’UNE NATION';

-- RECUPERATION DES RESULTATS D’UNE NATION ✅
select
	Country.countryName,
	Concat(firstName, " ", lastName) as "Athlete name"
from
	Athlete
	inner join Country on Country.idCountry = Athlete.idCountry
group by
	Athlete.idCountry,
	Athlete.firstName,
	Athlete.lastName;

select
	'-- EDITION DE PALMARES SUR UNE SAISON';

-- EDITION DE PALMARES SUR UNE SAISON ✅
select
	Concat(firstName, " ", lastName) as "Athlete name",
	Result.raceResult as "Rank",
	YEAR(Championship.championshipDateBeginning) as "Championship year"
from
	Athlete
	inner join AthletesResults on AthletesResults.idAthlete = Athlete.idAthlete
	inner join Result on Result.idResult = AthletesResults.idResult
	inner join Championship on Championship.idChampionship = Result.idChampionship
group by
	YEAR(Championship.championshipDateBeginning),
	raceResult,
	firstName,
	lastName
order by
	AthleteName;

select
	'-- EDITION DE « RECORDS »';

-- EDITION DE « RECORDS » ✅
select
	Concat(Athlete.firstName, " ", Athlete.lastName) as Athlete,
	Concat(raceDistance, "m") as Distance,
	Concat(
		Result.hValue,
		":",
		Result.mValue,
		"'",
		Result.sValue
	) as Duree,
	boatType
from
	Result
	inner join AthletesResults on AthletesResults.idResult = Result.idResult
	inner join Athlete on Athlete.idAthlete = AthletesResults.idAthlete
	inner join RaceResults on RaceResults.idResult = Result.idResult
	inner join Race on Race.idRace = RaceResults.idRace
	inner join BoatResults on BoatResults.idResult = Result.idResult
	inner join Boat on Boat.idBoat = BoatResults.idBoat
where
	Race.raceDistance = 200
group by
	boatType,
	Race.raceDistance,
	raceDistance,
	sValue,
	mValue,
	hValue,
	Athlete.firstName,
	Athlete.lastName
having
	sValue + 60 *(mValue + 60 * hValue) <= ALL (
		select
			sValue + 60 *(mValue + 60 * hValue)
		from
			Result
			inner join AthletesResults on AthletesResults.idResult = Result.idResult
			inner join Athlete on Athlete.idAthlete = AthletesResults.idAthlete
			inner join RaceResults on RaceResults.idResult = Result.idResult
			inner join Race on Race.idRace = RaceResults.idRace
			inner join BoatResults on BoatResults.idResult = Result.idResult
			inner join Boat on Boat.idBoat = BoatResults.idBoat
		group by
			Race.raceDistance,
			sValue,
			mValue,
			hValue
	);

select
	'-- TABLEAU DES MEDAILLES';

-- TABLEAU DES MEDAILLES ✅
select
	Concat(firstName, " ", lastName) as "Athlete name",
	CONCAT(
		"GOLD : ",
		count(
			case
				Result.raceResult
				when 1 then 1
				else null
			end
		)
	) as "AthleteGold",
	CONCAT(
		"SILVER : ",
		count(
			case
				Result.raceResult
				when 2 then 1
				else null
			end
		)
	) as "Athlete Silver",
	CONCAT(
		"BRONZE : ",
		count(
			case
				Result.raceResult
				when 3 then 1
				else null
			end
		)
	) as "Athlete Bronze"
from
	Athlete
	inner join AthletesResults on AthletesResults.idAthlete = Athlete.idAthlete
	inner join Result on Result.idResult = AthletesResults.idResult
	inner join Championship on Championship.idChampionship = Result.idChampionship
	inner join RaceResults on RaceResults.idResult = Result.idResult
	inner join Race on Race.idRace = RaceResults.idRace
where
	Result.raceResult = 1
	OR Result.raceResult = 2
	OR Result.raceResult = 3
group by
	Race.idRace,
	Athlete.idAthlete,
	sValue,
	mValue,
	hValue
order by
	sValue + 60 *(mValue + 60 * hValue) ASC;

select
	'-- ATHLETES LES PLUS TITRES';

-- ATHLETES LES PLUS TITRES
select
	Concat(firstName, " ", lastName) as "Athlete name",
	COUNT(*) as "N times winner"
from
	Athlete
	inner join AthletesResults on AthletesResults.idAthlete = Athlete.idAthlete
	inner join Result on Result.idResult = AthletesResults.idResult
	inner join Championship on Championship.idChampionship = Result.idChampionship
	inner join RaceResults on RaceResults.idResult = Result.idResult
	inner join Race on Race.idRace = RaceResults.idRace
where
	Result.raceResult = 1
group by
	Athlete.idAthlete
Having
	count(*) >= ALL(
		select
			COUNT(*)
		from
			Athlete
			inner join AthletesResults on AthletesResults.idAthlete = Athlete.idAthlete
			inner join Result on Result.idResult = AthletesResults.idResult
			inner join Championship on Championship.idChampionship = Result.idChampionship
			inner join RaceResults on RaceResults.idResult = Result.idResult
			inner join Race on Race.idRace = RaceResults.idRace
		where
			Result.raceResult = 1
		group by
			Athlete.idAthlete
	);

select
	'-- RECUPERATION DES CLASSEMENTS DES JOUEURS';

-- RECUPERATION DES CLASSEMENTS DES JOUEURS ✅
set
	@row_num = 0;

select
	T1.*,
	@row_num := @row_num + 1 as row_index
from
	(
		select
			CONCAT(User.firstName, " ", User.lastName) as UserName,
			100 / AVG(Result.raceResult) as Points
		from
			User
			inner join Team on Team.idUser = User.idUser
			inner join TeamAthletes on TeamAthletes.idTeam = Team.idTeam
			inner join Athlete on Athlete.idAthlete = TeamAthletes.idAthlete
			inner join AthletesResults on AthletesResults.idAthlete = Athlete.idAthlete
			inner join Result on Result.idResult = AthletesResults.idResult
		group by
			User.idUser
		order by
			Points DESC
	) as T1;

select
	'-- CLASSEMENT DES EQUIPES';

-- CLASSEMENT DES EQUIPES ✅
select
	CONCAT(User.firstName, " ", User.lastName) as UserName,
	100 / AVG(Result.raceResult) as Points
from
	User
	inner join Team on Team.idUser = User.idUser
	inner join TeamAthletes on TeamAthletes.idTeam = Team.idTeam
	inner join Athlete on Athlete.idAthlete = TeamAthletes.idAthlete
	inner join AthletesResults on AthletesResults.idAthlete = Athlete.idAthlete
	inner join Result on Result.idResult = AthletesResults.idResult
group by
	User.idUser
order by
	Points ASC;

select
	'-- AFFICHAGE DU DETAIL D’UNE EQUIPE';

-- AFFICHAGE DU DETAIL D’UNE EQUIPE ✅
select
	Team.teamName,
	Team.idTeam,
	CONCAT(Athlete.firstName, " ", Athlete.lastName) as "Athlete Name"
from
	Team
	inner join TeamAthletes on TeamAthletes.idTeam = Team.idTeam
	inner join Athlete on Athlete.idAthlete = TeamAthletes.idAthlete
group by
	Team.idTeam,
	Athlete.firstName,
	Athlete.lastName;

select
	'-- DECOUVERTE D’UN SPORTIF AVEC METADATA';

-- DECOUVERTE D’UN SPORTIF AVEC METADATA ✅
select
	firstName,
	lastName,
	athletesMetadataValue,
	athletesMetadataDescription,
	countryName
from
	Athlete
	inner join AthletesMetadata on AthletesMetadata.idAthlete = Athlete.idAthlete
	inner join Country on Country.idCountry = Athlete.idCountry
ORDER BY
	RAND()
LIMIT
	1;

select
	'-- DECOUVERTE D’UN SPORTIF';

-- DECOUVERTE D’UN SPORTIF ✅
select
	firstName,
	lastName,
	countryName
from
	Athlete
	inner join Country on Country.idCountry = Athlete.idCountry
ORDER BY
	RAND()
LIMIT
	1;